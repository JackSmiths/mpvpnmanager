//
//  ViewController.m
//  MPVPNManager
//
//  Created by mopellet on 2017/5/30.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import "ViewController.h"

#import "MPVPNManager.h"

@interface ViewController()
@property (nonatomic, strong) MPVPNManager * mpVpnManager;
@property (weak, nonatomic) IBOutlet UILabel *describe;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //  https://github.com/MoPellet  更多项目详情点击进入
    //  my blog http://pkwans.com/
    // 您的star就是对我开发最大的动力
    
    
#warning first to do
    // 1，在开发者网站，新增一个 AppID，并开启 Network Extensions 和 Personal VPN 的服务
    // 2，修改 AppID 为已经开启上述两个服务的 AppID
    // 3，在工程的 capabilities 中开启 Personal VPN
    // 4，为当前工程，导入 NetworkExtension.framework 和 Security.framework
    // 5, 设置vpnconfig 一定要设置 VPNConnectType
    // 6, 根据设置的参数自动验证
    
    _mpVpnManager = [MPVPNManager shareInstance];
    
    [_mpVpnManager checkVPNConfigCompleteHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"success:%d error:%@",success,error.localizedDescription);
    }];
    
    /**VPN连接状态的改变**/
    [_mpVpnManager startMonitoringVPNStatusDidChange:^(enum NEVPNStatus status) {
        [self updateTitle];
        NSLog(@"status change:%ld",(long)status);
        // 在这里知道VPN是否开启
        // 检测IP是否是你代理设置的IP
        
        NSLog(@"currentVPNConnectConfig:%@",_mpVpnManager.currentVPNConnectConfig);
    }];
    
    //如果提示vpn服务器并未响应是配置账号的问题 请优先确保账号正确型，可在macOS或者iOS系统自带的VPN测试
    //自行填入正确的账号密码测试
    /*
     * 服务端可以用 strongswan https://www.strongswan.org/
     * 服务端配置可参考 https://raymii.org/s/tags/vpn.html
     */
    [self startIPSec];
//    [self startIKEv2];
    
    // 检测IP是否是你代理设置的IP (VPN 是否为当前VPN)
    [_mpVpnManager checkVPNConfigCompleteHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"checkVPNConfigCompleteHandler:%d error:%@",success,error);
    }];
}

- (void)startIPSec {
    //初始化配置信息
    /**共享秘钥方式*/
    MPVPNConfigInfo *config = [MPVPNConfigInfo new];
    config.configTitle = @"MPVPNManager";
    config.VPNConnectType = MPVPNConnectTypeIPSec;
    config.serverAddress = @"xxx.xxx.xxx.xxx";
    config.username = @"username";
    config.password = @"password";
    config.sharePrivateKey = @"sharePrivateKey";
    _mpVpnManager = [MPVPNManager shareInstance];
    [_mpVpnManager settingUpVPNConfig:config completeHandler:^(BOOL success, NSError * _Nullable error) {
       NSLog(@"save IPSec config %@ error:%@",success?@"success":@"fail",error); 
    }
     ];
}

- (void)startIKEv2 {
    /**以下方式需要安装pem描述文件*/
    /**不需要验证信息方式*/
    /**直接用AirDrop将CACert.pem发送到手机即可*/
    MPVPNConfigInfo *config = [MPVPNConfigInfo new];
    config.configTitle = @"MPVPNManager";
    config.VPNConnectType = MPVPNConnectTypeIKEv2;
    config.serverAddress = @"xxx.xxx.xxx.xxx";
    config.username = @"username";
    config.password = @"password";
    config.remoteID = @"remoteID";
    config.serverCertificateCommonName = @"StrongSwan Root CA";
    config.serverCertificateIssuerCommonName = @"StrongSwan Root CA";
    _mpVpnManager = [MPVPNManager shareInstance];
    [_mpVpnManager settingUpVPNConfig:config completeHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"save IKEv2 config %@ errot:%@",success?@"success":@"fail",error);
    }];
}

- (void)iOS9Test {
    // ios 9 参考
    NETunnelProviderManager * manager = [[NETunnelProviderManager alloc] init];
    NETunnelProviderProtocol * protocol = [[NETunnelProviderProtocol alloc] init];
    protocol.providerBundleIdentifier = @"com.mopellet.Vpn";
    
    protocol.providerConfiguration = @{@"key":@"value"};
    protocol.serverAddress = @"server";
    manager.protocolConfiguration = protocol;
    [manager saveToPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        
    }];
    
    NETunnelProviderSession * session = (NETunnelProviderSession *)manager.connection;
    NSDictionary * options = @{@"key" : @"value"};
    
    NSError * err;
    [session startTunnelWithOptions:options andReturnError:&err];
}

- (IBAction)start:(id)sender {
    [_mpVpnManager startVPNConnectCompletionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
}

- (IBAction)stop:(id)sender {
    [_mpVpnManager stopVPNConnectCompletionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
}


- (void)updateTitle{
    NSString * string = @"Invalid";
    switch ([_mpVpnManager currentVPNStatus]) {
        case NEVPNStatusInvalid:
            string = @"Invalid";
            break;
        case NEVPNStatusDisconnected:
            string = @"Disconnected";
            break;
        case NEVPNStatusConnecting:
            string = @"Connecting";
            break;
        case NEVPNStatusConnected:
            string = @"Connected";
            break;
        case NEVPNStatusReasserting:
            string = @"Reasserting";
            break;
        case NEVPNStatusDisconnecting:
            string = @"Disconnecting";
            break;
    }
    _describe.text = string;
}

@end
